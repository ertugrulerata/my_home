{ pkgs ? import <nixpkgs> {}
}:
pkgs.mkShell {
   name="dev-environment";
   buildInputs = [
   pkgs.htop
   ];
   shellHook = ''
   echo "Start developing..."
   '';
}
